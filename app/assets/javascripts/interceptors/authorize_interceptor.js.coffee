angular.module('lunchTime').factory 'authorizeInterceptor', Array "$q", "$rootScope", ($q, $rootScope) ->
  return {
    responseError: (rejection)->
      if rejection.status == 401
        $rootScope.unauthorized = true
      $q.reject(rejection)
  }