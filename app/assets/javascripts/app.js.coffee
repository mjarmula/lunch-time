angular.module('lunchTime', ['ui.router', 'templates', 'rails', 'ngFlash'])
.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$httpProvider'
  ($stateProvider, $urlRouterProvider, $httpProvider)->
    $stateProvider.state('home',{
        url: '/home'
        templateUrl: 'home/index.html',
        controller: 'HomeController'
    })
    $stateProvider.state('orders',{
        url: '/orders'
        templateUrl: 'order/views/index.html',
        controller: 'OrderListController'
        controllerAs: 'orderListCtrl'
    })
    $stateProvider.state('showOrder',{
        url: '/order/:id'
        templateUrl: 'order/views/show.html',
        controller: 'OrderController'
        controllerAs: 'orderCtrl'
    })
    $stateProvider.state('editOrder',{
        url: '/order/:id/edit'
        templateUrl: 'order/views/edit.html',
        controller: 'OrderController'
        controllerAs: 'orderCtrl'
    })
    $stateProvider.state('newMeal',{
        url: '/orders/:order_id/meal/new'
        templateUrl: 'meal/views/new.html',
        controller: 'MealController'
        controllerAs: 'mealCtrl'
    })
    $urlRouterProvider.otherwise('home');
    $httpProvider.interceptors.push('authorizeInterceptor')
])