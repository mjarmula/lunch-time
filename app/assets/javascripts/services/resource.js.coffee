angular.module('lunchTime').factory "Resource", Array 'RailsResource', 'Flash', '$q', (RailsResource, Flash, $q) ->
  class Resource extends RailsResource
    @excluded_attrs: ["user_id", 'base']
    @configure
      interceptors: [{
        afterResponseError: (rejection, resourceConstructor, context) =>
          if angular.isObject(rejection.data)
            for attr, obj of rejection.data
              for message in obj
                message = if attr not in @excluded_attrs then attr + ' ' + message else message
                Flash.create('danger', message)
              
          return $q.reject(rejection)
        ,
        afterResponse: (result, resourceConstructor, context)->
          return result;
      }] 
    
    update: ->
      super.then((data)=>
        Flash.create('success', @__proto__.constructor.name + ' has been updated successfully')
        return data
      )
      
    create: ->
      super.then((data)=>
        Flash.create('success', @__proto__.constructor.name + ' has been saved successfully')
        return data
      )
      
    delete: ->
      super.then((data)=>
        Flash.create('warning', @__proto__.constructor.name + ' has been destroy successfully')
        return data
      )