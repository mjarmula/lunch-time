class Auth
  @$inject: ['$http', '$rootScope']
  constructor: ($http, @rootScope)->
    @user = {}
    $http.get('api/current_user')
    .then((data)=>
      angular.extend @user, data.data if data
    )
    return @
  current_user: ->
    @user
    

angular.module('lunchTime').service 'Auth', Auth