angular.module('lunchTime').factory "Meal", ['Resource', (Resource) ->
    class Meal extends Resource
      @configure
        url: '/api/meals',
        name: 'meal'
]