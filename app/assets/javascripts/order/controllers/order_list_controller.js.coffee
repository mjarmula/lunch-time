class OrderListController
  @$inject: ['Order','$scope']
  constructor: (@Order, $scope)->
    @scope = $scope
    @Order.query().then((data)=>
        @orders = data
    )
      
  save: ->
    new @Order(@scope.orderListCtrl.order).create().then( (data) => 
      @scope.orderListCtrl.orders.push(data)
      @order = {}
      @new = false
    )
  
  delete: (order)->
    order.delete().then(=>
      index = @scope.orderListCtrl.orders.indexOf(order)
      @scope.orderListCtrl.orders.splice index, 1
    )
    
angular.module('lunchTime').controller 'OrderListController', OrderListController