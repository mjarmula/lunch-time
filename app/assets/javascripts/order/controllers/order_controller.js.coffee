class OrderController
  @$inject: ['Order','Meal','$scope','$state','Flash']
  constructor: (@Order,@Meal,@scope,@state,@Flash)->
    @meals = []
    @Order.get(@state.params.id).then((data)=>
      @scope.orderCtrl.order= data
      @meals = data.meals
    )
    
  update: ->
    @order.update()
  
  add_meal: ->
    @scope.orderCtrl.meal.order_id = @state.params.id
    new @Meal({meal: @scope.orderCtrl.meal}).create().then((data) =>
        @scope.orderCtrl.meal = {}
        @scope.orderCtrl.new_meal = false
        @scope.orderCtrl.meals.push(data)
    )
    
  destroy_meal: (meal)->
    meal.delete().then(=>
      index = @scope.orderCtrl.meals.indexOf(meal)
      @scope.orderCtrl.meals.splice index, 1
    )
    
angular.module('lunchTime').controller 'OrderController', OrderController