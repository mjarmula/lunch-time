angular.module('lunchTime').factory "Order", Array 'Resource','railsSerializer', (Resource,railsSerializer) ->
    class Order extends Resource
      @configure
        url: '/api/orders',
        name: 'order',
        serializer: railsSerializer( ->
          @nestedAttribute('meal')
          @resource('meals', 'Meal')
        )
    
      order_class: ->
        switch @status
          when 'ordered' then return 'success'
          when 'delivered' then return 'warning'
          when 'finalized' then return 'danger'
          
      getTotal: ->
        total = v.price + v.price for k, v of @meals
        return parseFloat(total)