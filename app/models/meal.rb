class Meal < ApplicationRecord
  belongs_to :user
  belongs_to :order
    
  validates :name, :price, :user_id, :order_id, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0.01 }
  validates :user_id, uniqueness: { scope: :order_id, message: "You can add only one meal"}
  validate :order_unfinalized
  
private

  def order_unfinalized
    errors.add(:base, 'Order is closed') if order.finalized?
  end
  
  def created_at
    read_attribute(:created_at).strftime('%Y-%m-%d %H:%M')
  end
end
