module Filterable
  extend ActiveSupport::Concern
  def filter(filters)
    result = self
    filters.each do |filter_name, filter_args|
      if filter_args and filter_args.present?
        result = self.send(filter_name, filter_args)
      else
        result = self.send(filter_name)
      end
    end
    return result
  end
  
  module ClassMethods
    def filter(filters)
      result = all
      filters.each do |filter_name, filter_args|
        if filter_args and filter_args.present?
          result = result.send(filter_name, filter_args)
        else
          result = result.send(filter_name)
        end
      end
      return result
    end
  end
end