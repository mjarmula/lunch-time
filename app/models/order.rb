class Order < ApplicationRecord
  include ActiveModel::Serialization  
  
  enum status: {
    'ordered' => 0,
    'delivered' => 1,
    'finalized' => 2
  }
  
  has_many :meals, dependent: :destroy
  has_many :users, through: :meals
  
  validates :restaurant_name, :status, presence: true
  validates :status, inclusion: {in: self.statuses.keys}
  
  attr_reader :statuses
  
  def statuses
    self.class.statuses.invert
  end

  def attributes
    super.merge({'statuses' => 'statuses'})
  end

  def created_at
    read_attribute(:created_at).strftime('%Y-%m-%d')
  end
  
end
