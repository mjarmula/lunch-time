class User < ApplicationRecord
  include Filterable
  enum provider: {
    "facebook" => 0
  }
  
  validates :name, :uid, :image_url, presence: true
  has_many :meals, dependent: :destroy
  has_many :orders, through: :meals
  
  def self.from_omniauth(data)
    user = User.find_or_create_by(uid: data['uid'])
    user.uid = data['uid']
    user.name = data['info']['name']
    user.image_url = data['info']['image']
    user.save
    user.try("#{data['provider']}!")
    user
  end
end