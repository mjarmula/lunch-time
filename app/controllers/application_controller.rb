class ApplicationController < ActionController::Base
  include Authenticate
  protect_from_forgery with: :exception
  before_action :authenticate
end