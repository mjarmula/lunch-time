class SessionsController < ApplicationController
  skip_before_action :authenticate, on: [:create]
  
  def user
    render json: current_user
  end
  
  def create
    user = User.from_omniauth(request.env['omniauth.auth'])
    session[:user_id] = user.id
    redirect_to root_path
  end
  
  def destroy
    session.delete :user_id
    redirect_to root_path
  end
end