module Authenticate
  extend ActiveSupport::Concern
  
  included do
    helper_method :current_user
    after_action :set_csrf_cookie_for_ng
  end
protected

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end
  
  def authenticate
    head :unauthorized if not current_user
  end
  
  def current_user
    User.find_by(id: session[:user_id])
  end
end