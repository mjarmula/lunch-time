FactoryGirl.define do
  factory :meal do
    name "MyString"
    price "9.99"
    association :user, factory: :user
    association :order, factory: :order
  end
end
