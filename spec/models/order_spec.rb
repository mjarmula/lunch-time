require 'rails_helper'

RSpec.describe Order, :type => :model do
  describe  'associations' do
    it { is_expected.to have_many :meals }
    it { is_expected.to have_many(:users).through(:meals) }
  end
  
  describe 'validations' do
    it {is_expected.to validate_presence_of :restaurant_name}
  end
end