require 'rails_helper'

RSpec.describe User, :type => :model do
  describe 'associations' do
    it { is_expected.to have_many(:orders).through(:meals) }
  end
  
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :uid }
    it { is_expected.to validate_presence_of :image_url }
  end
end
