require 'rails_helper'

RSpec.describe Meal, :type => :model do
  describe 'associations' do
    it { is_expected.to belong_to :user }
    it { is_expected.to belong_to :order }
  end
end
