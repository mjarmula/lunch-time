Rails.application.routes.draw do
  root "home#start"
  
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'logout', to: 'sessions#destroy'
  
  scope '/api' do
    resources :users
    resources :orders
    resources :meals
    get 'current_user', to: 'sessions#user'
  end
end